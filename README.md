# cordova-plugin-notch-detect

Cordova plugin that can detect if the device has a notch/display cutout.

## Installation

```bash
$ cordova plugin add git+https://bitbucket.org/skadit/cordova-plugin-notch-detect.git --save
# Or
$ cordova plugin add https://bitbucket.org/skadit/cordova-plugin-notch-detect.git --save
```

## Compatibility

- `cordova-android` >= `8.0.0`
- `cordova-ios`

## API

### iOS

#### hasNotch

`NotchDetection.hasNotch(callbackFn)`

`callbackFn` returns a single boolean argument - whether the device has a notch.

### Android

#### hasDisplayCutout

`NotchDetection.hasDisplayCutout(callbackFn)`

`callbackFn` returns a single boolean argument - whether the device has a display cutout.
If the device's API level is below 28 (Android P), this is always false.

#### getSafeInsetBottom

`NotchDetection.getSafeInsetBottom(callbackFn)`

`callbackFn` returns a single integer argument - the safe inset bottom value.

#### getSafeInsetTop

`NotchDetection.getSafeInsetTop(callbackFn)`

`callbackFn` returns a single integer argument - the safe inset top value.

#### setDisplayCutoutLayoutShortEdges

`NotchDetection.setDisplayCutoutLayoutShortEdges(callbackFn)`

`callbackFn` does not return anything.
