
var exec = require('cordova/exec');

var PLUGIN_NAME = 'NotchDetection';

var NotchDetection = {
    hasNotch: function(callback) {
        exec(callback, null, PLUGIN_NAME, 'hasNotch', []);
    },

    hasDisplayCutout: function(callback) {
        exec(callback, null, PLUGIN_NAME, 'hasDisplayCutout', []);
    },

    getSafeInsetBottom: function(callback) {
        exec(callback, null, PLUGIN_NAME, 'getSafeInsetBottom', []);
    },

    getSafeInsetTop: function(callback) {
        exec(callback, null, PLUGIN_NAME, 'getSafeInsetTop', []);
    },

    setDisplayCutoutLayoutShortEdges: function(callback) {
        exec(callback, null, PLUGIN_NAME, 'setDisplayCutoutLayoutShortEdges', []);
    }
};

module.exports = NotchDetection;
