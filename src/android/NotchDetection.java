package org.apache.cordova.notchdetection;

import android.app.Activity;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.DisplayCutout;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONException;

public class NotchDetection extends CordovaPlugin {
    private static final String PLUGIN_NAME = "NotchDetection";

    private static final String ACTION_HAS_DISPLAY_CUTOUT = "hasDisplayCutout";
    private static final String ACTION_GET_SAFE_INSET_TOP = "getSafeInsetTop";
    private static final String ACTION_GET_SAFE_INSET_BOTTOM = "getSafeInsetBottom";
    private static final String ACTION_SET_DISPLAY_CUTOUT_LAYOUT_SHORT_EDGES = "setDisplayCutoutLayoutShortEdges";

    private Activity activity;
    private Window window;
    private View decorView;

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    @Override
    public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
        LOG.v(PLUGIN_NAME, String.format("%s: initialization", PLUGIN_NAME));
        super.initialize(cordova, webView);

        this.cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Clear flag FLAG_FORCE_NOT_FULLSCREEN which is set initially
                // by the Cordova.
                Window window = cordova.getActivity().getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            }
        });
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArray of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false otherwise.
     */
    @Override
    public boolean execute(final String action, final CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        LOG.v(PLUGIN_NAME, String.format("Executing action: %s", action));

        activity = cordova.getActivity();
        window = activity.getWindow();
        decorView = window.getDecorView();

        if (ACTION_HAS_DISPLAY_CUTOUT.equals(action)) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean hasDisplayCutout = hasDisplayCutout();
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, hasDisplayCutout));
                }
            });
            return true;
        }

        if (ACTION_GET_SAFE_INSET_TOP.equals(action)) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int getSafeInsetTop = getSafeInsetTop() / getDisplayDensity();
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getSafeInsetTop));
                }
            });
            return true;
        }

        if (ACTION_GET_SAFE_INSET_BOTTOM.equals(action)) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int getSafeInsetBottom = getSafeInsetBottom() / getDisplayDensity();
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getSafeInsetBottom));
                }
            });
            return true;
        }

        if (ACTION_SET_DISPLAY_CUTOUT_LAYOUT_SHORT_EDGES.equals(action)) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setDisplayCutoutLayoutShortEdges();
                    callbackContext.success();
                }
            });
            return true;
        }

        return false;
    }

    private boolean hasDisplayCutout() {
        if (deviceIsRunningPie()) {
            DisplayCutout displayCutout = decorView.getRootWindowInsets().getDisplayCutout();

            return displayCutout != null;
        }
        return false;
    }

    private int getSafeInsetTop() {
        if (deviceIsRunningPie()) {
            DisplayCutout displayCutout = decorView.getRootWindowInsets().getDisplayCutout();

            if (displayCutout != null) {
                return displayCutout.getSafeInsetTop();
            }
        }

        return 0;
    }

    private int getSafeInsetBottom() {
        if (deviceIsRunningPie()) {
            DisplayCutout displayCutout = decorView.getRootWindowInsets().getDisplayCutout();

            if (displayCutout != null) {
                return displayCutout.getSafeInsetBottom();
            }
        }

        return 0;
    }

    /**
     * Sets the application flag so that the application extends into the
     * display cutout. Works only on Android P (API level 28) devices.
     *
     * See: https://developer.android.com/reference/android/view/WindowManager.LayoutParams.html#LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
     */
    private void setDisplayCutoutLayoutShortEdges() {
        if (deviceIsRunningPie()) {
            WindowManager.LayoutParams attributes = window.getAttributes();

            attributes.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
    }

    private boolean deviceIsRunningPie() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.P;
    }

    /**
     * Returns the device display density.
     *
     * See: https://developer.android.com/reference/android/util/DisplayMetrics.html#density
     */
    private int getDisplayDensity() {
        DisplayMetrics metrics = new DisplayMetrics();
        window.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return (int)(metrics.density);
    }
}
