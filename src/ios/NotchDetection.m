#import "NotchDetection.h"

#import <Cordova/CDVAvailability.h>

@implementation NotchDetection {
    NSArray *MODEL_INTERNAL_NAMES_WITH_NOTCH;
}

- (void)pluginInitialize {
    if (!MODEL_INTERNAL_NAMES_WITH_NOTCH) {
        // See: https://gist.github.com/adamawolf/3048717
        MODEL_INTERNAL_NAMES_WITH_NOTCH = [NSArray arrayWithObjects: @"iPhone10,3", @"iPhone10,6", @"iPhone11,8", @"iPhone11,2", @"iPhone11,6", @"iPhone11,4", @"iPhone12,1", @"iPhone12,3", @"iPhone12,5", @"iPhone13,1", @"iPhone13,2", @"iPhone13,3", @"iPhone13,4", nil];
    }
}

- (void)hasNotch:(CDVInvokedUrlCommand *)command {
    static BOOL hasNotch = NO;
#if TARGET_IPHONE_SIMULATOR
    NSString *model = NSProcessInfo.processInfo.environment[@"SIMULATOR_MODEL_IDENTIFIER"];
#else
    struct utsname systemInfo;
    uname(&systemInfo);

    NSString *model = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
#endif

    hasNotch = [MODEL_INTERNAL_NAMES_WITH_NOTCH containsObject:model];

    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:hasNotch];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

@end
