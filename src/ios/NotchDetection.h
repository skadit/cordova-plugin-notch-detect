#import <sys/utsname.h>
#import <Cordova/CDVPlugin.h>

@interface NotchDetection : CDVPlugin {
}

// The hooks for our plugin commands
- (void)hasNotch:(CDVInvokedUrlCommand *)command;

@end
